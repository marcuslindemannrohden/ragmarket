//
//  MRConnection.swift
//
//  Created by Marcus Lindemann Rohden on 10/6/18.
//  Copyright © 2018 Marcus Lindemann Rohden. All rights reserved.
//

import Alamofire
import Firebase


class MRConnection: NSObject {
    
    var headerToJson: HTTPHeaders = [:]
    
    var headerToJSON_String: [String:String]?
    var urlToJSON: NSString?
    var serverToUse: Server?
    var ref: DatabaseReference!
    
    
    enum Server{
        case sea
        case global
    }
    
    
    static let shared = MRConnection()
    
    private override init(){
        super.init()
        
        self.serverToUse = Server.global
        self.ref = Database.database().reference()
        self.setServer()
    }
    
    
    
    /*
     FALTA ADICIONAR O SET USERDEFAULTS
     */
    func setServer(){
        if let tempServer = serverToUse{
            switch tempServer {
            case .sea:
                self.headerToJSON_String = ["Origin":"https://poporing.life"]
                self.urlToJSON = "https://api.poporing.life/"
            case .global:
                self.headerToJSON_String = ["Origin":"https://global.poporing.life"]
                self.urlToJSON = "https://api-global.poporing.life/"
            }
        }
    }
    
    
    func getAllItemListFromFirebase(){
        ref.observe(.value, with: { snapshot in
            print(snapshot.value as Any)
        })
    }
    
    
    
    
}
