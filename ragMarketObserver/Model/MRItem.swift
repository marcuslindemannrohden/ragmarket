//
//  MRItem
//
//  Created by Marcus Lindemann Rohden on 11/6/18.
//  Copyright © 2018 Marcus Lindemann Rohden. All rights reserved.
//

import UIKit

class MRItem: NSObject {
    
    var name: NSString
    var cost: NSInteger
    var volume: NSInteger
    
    
    override init(){
        self.name = ""
        self.cost = -1
        self.volume = -1
    }
    
}
